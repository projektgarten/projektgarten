# Projektgruppe 
## Willkommen 
Herzlich Willkommen!
Super, dass Du auf unserer Seite gelandet bist!

Dieses Dokument beinhaltet eine Übersicht unseres Projekts:
* [Was wir machen](#waswirmachen)
* [Wer wir sind](#werwirsind)
* [Was wir brauchen](#waswirbrauchen)
* [Mach mit!](#machmit)
* [Kontaktier uns!](#kontakt)
* [Querverweise](#querverweise)

## Was wir machen <a name="waswirmachen">
Willkommen ist jeder, der teilnehmen will, eine Idee hat oder sich engagieren will. 
Dadurch erreichen wir eine bessere Sichtbarkeit von freien Projekten und erzielen ein besseres interdiziplinäres Angebot durch Zusammenarbeit vieler verschiedener Akteure, sowohl aus den einzelnen Studiengängen, als auch von außerhalb.

### Das Problem
* Nachhaltige Entwicklung
* Politische Entwicklung
* Freie Projekte bekommen keine Aufmerksamkeit
* wenig interdisziplinäre Zusammenarbeit
* klassische Hierarchie

### Die Lösung
Wir bieten Raum für:
Freiere, eigeninitiierte Projektentwicklung
Interdiszipinäre, nicht hierarchische Schaffung von Interessengruppen.
* Offener, ständig wachsender Informationspool
* Ideen teilen
* Zusammenarbeit
* Feedback
* Interessenfelder
* Vernetzung


## Wer wir sind <a name="werwirsind">
Den Anfang machten Studierende, Personen der Verwaltung und Lehrende der Hochschule der Bildenden Künste in Saarbrücken. Das Projekt ist für jeden Interessierten offen. 

## Was wir brauchen <a name="waswirbrauchen">
Leute die den Dialog suchen und ihre Projekte an uns herantragen, sie eigenständig umsetzen und ihre Ergebnisse teilen wollen. Mundpropaganda, Sichtbarkeit an der Hochschule (Beete), HBK-Webseite, Gitlab, Publikationen, Rundgang, lokales Netzwerk (Transition, sevengardens, Stadtbauernhof, Über den Tellerrand, andere Bildungsinstitutionen …)

## Mach mit! <a name="machmit">
Triff dich jeden ersten und dritten Donnerstag mit uns am Projektgarten und teile deine freien Projekte, Ideen und Anliegen um andere Interessenten zu finden oder Feedback zu bekommen. Auf der gitlab Seite kannst du deine Projekte und Erkenntnisse teilen damit sie geshared und weiter ausgebaut werden können.

## Kontaktier uns! <a name="kontakt">
Am besten indem du direkt bei einem unserer Treffen vorbeischaust jeden 1. und 3. Donnerstag an den Beeten der Hochschule der Bildenden Künste in Saarbrücken.

## Querverweise <a name="querverweise">
Informationen, Bildergalerie und einzelne Projekte findest du auf der [HBK-Website](https://www.hbksaar.de/startseite) unter den Schlagwörtern Projektgarten.
Auf unserem GitLab findest du die Projektdokumentationen, Arbeitsergebnisse und kannst deine eigenen Erfahrungen und Prozesse teilen. 

## Danke!
Vielen Dank, dass du es rumerzählst. Wir würden uns freuen dich bald mal in einem unserer Treffen kennenzulernen und von deinen Ideen zu hören.

## Verzeichnis 
README file: ein Dokument welches ein offenes Projekt den Leuten und potentiellen Interessenten die am Mitmachen interessiert sind vorzustellen.
Projektgarten: Ein Garten mit Pflanzen aus denen man Farben herstellen kann. Diese können für Projekte genutzt werden. Sie werden gemeinsam von den Studenten gepflegt.